    select  
    isnull(LTRIM(RTRIM(CLV_ART)), '') as code,
    '' as barCode,
    '' name,
    isnull(DESCR,'') description,
    isnull(FCH_ULTCOM, '') lastPurchase,
    status,
    cast(PRECIO1 as varchar(20)) +'|' + cast(PRECIO2 as varchar(20)) + '|' + cast(PRECIO3 as varchar(20)) price,
    isnull(ltrim(rtrim(CVEESQIMP)),'') as tax,
    isnull(LIN_PROD,'') as category,
    isnull(UNI_MED,'') as unit,
    isnull(EXIST,0) as availableQuantity,
    isnull(ULT_COSTO,0) as unitCost
    
     from inve01