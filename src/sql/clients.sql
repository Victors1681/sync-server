select  
    ltrim(rtrim( CCLIE )) as code,
    ltrim(rtrim( NOMBRE )) as name,
     ltrim(rtrim( RFC )) as identification,
     ltrim(rtrim( EMAIL )) as email,
     ltrim(rtrim( TELEFONO )) as phonePrimary,
     ltrim(rtrim( TELEFONOS )) as phoneSecondary,
      ltrim(rtrim( FAX )) as fax,
     '' as  mobile,
      ltrim(rtrim( observacion )) as observations, 
    ltrim(rtrim( DIR )) as address,
     ltrim(rtrim( POB )) as city,
      ltrim(rtrim( COLONIA )) as state,
      '' as zipCode,
     'Republica Dominicana' as country, 
    ltrim(rtrim( VEND )) as sellerId,

    SALDO as balance,
     0  as creditLimit,
    '0' as priceListId,
    ltrim(rtrim( STATUS )) as status

    from clie01 where NOMBRE != ''