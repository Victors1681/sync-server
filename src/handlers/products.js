import mssqlQuery from "../connections/mssql";
import { printPayloadSize } from "../utils/utils";

const createProductRow = row => {
  const {
    code,
    barCode,
    name,
    description,
    lastPurchase,
    status,
    price,
    tax,
    category,
    unit,
    availableQuantity,
    unitCost
  } = row;

  const priceList = price.split("|").map((p, i) => ({
    idPriceList: `${i}`,
    name: `${i}`,
    price: parseFloat(p)
  }));
  const taxList = tax.split("|").map(t => ({ id: t }));
  const categoryList = { id: category, name: category, description: category };
  const inventory = {
    unit,
    availableQuantity,
    unitCost
  };

  return {
    code,
    barCode,
    name,
    description,
    lastPurchase,
    status: status === "A",
    price: priceList,
    tax: taxList,
    category: categoryList,
    inventory,
    fromSync: true
  };
};

module.exports = async sql => {
  try {
    const result = await mssqlQuery(sql.products);
    const payload = result.map(createProductRow);

    printPayloadSize(payload);

    return payload;
  } catch (err) {
    console.log("Product error: ", err);
    throw new Error(err);
  }
};
