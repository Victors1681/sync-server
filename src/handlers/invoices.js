import { mssqlExecute } from "../connections/mssql";
import { printPayloadSize } from "../utils/utils";

const createSellerRow = row => {
  const {
    id,
    name,
    identification,
    observation,
    email,
    classification,
    status
  } = row;

  return {
    id,
    name,
    identification,
    observation,
    email,
    classification,
    status: status === "A" ? true : false,
    fromSync: true //<----- Bulk Sync
  };
};

module.exports = async sql => {
  try {
    console.log("STARTINGGG>..");
    const result = await mssqlExecute("Stp_consultasaldoclientes_global");

    console.log(result);
    const payload = result;
    //const payload = result.map(createSellerRow);
    printPayloadSize(payload);

    return payload;
  } catch (err) {
    console.log("Client Error:", err);
    throw new Error(err);
  }
};
