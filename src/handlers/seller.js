import mssqlQuery from "../connections/mssql";
import { printPayloadSize } from "../utils/utils";

const createSellerRow = row => {
  const {
    id,
    name,
    identification,
    observation,
    email,
    classification,
    status
  } = row;

  return {
    id,
    name,
    identification,
    observation,
    email,
    classification,
    status: status === "A" ? true : false,
    fromSync: true //<----- Bulk Sync
  };
};

module.exports = async sql => {
  try {
    const result = await mssqlQuery(sql.sellers);

    const payload = result.map(createSellerRow);
    printPayloadSize(payload);

    return payload;
  } catch (err) {
    console.log("Client Error:", err);
    throw new Error(err);
  }
};
