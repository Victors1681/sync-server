import mssqlQuery from "../connections/mssql";
import { printPayloadSize } from "../utils/utils";

const createClientRow = row => {
  const {
    code,
    name,
    identification,
    email,
    phonePrimary,
    phoneSecondary,
    fax,
    mobile,
    observations,
    address,
    city,
    state,
    zipCode,
    country,
    sellerId,
    balance,
    creditLimit,
    priceListId,
    status
  } = row;

  return {
    code,
    name,
    identification,
    email,
    phonePrimary,
    phoneSecondary,
    fax,
    mobile,
    observations,
    address: {
      address,
      city,
      state,
      zipCode,
      country
    },
    financial: {
      balance,
      creditLimit
    },
    sellerId,
    priceListId,
    status,
    fromSync: true
  };
};

module.exports = async sql => {
  try {
    const result = await mssqlQuery(sql.clients);

    const payload = result.map(createClientRow);

    printPayloadSize(payload);

    return payload;
  } catch (err) {
    console.log("Client Error:", err);
    throw new Error(err);
  }
};
