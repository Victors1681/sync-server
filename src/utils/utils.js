module.exports.printPayloadSize = payload => {
  console.log(
    `Size: ${Buffer.byteLength(JSON.stringify(payload), "utf8") / 1000000} MB`
  );
};
