import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";
import localStorage from "localStorage";
import fetch from "node-fetch";

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("token");
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token.toString()}` : ""
    }
  };
});

module.exports = async () => {
  try {
    const httpLink = new HttpLink({
      uri: `${process.env.HOST}:${process.env.PORT}/graphql`,
      fetch
    });

    const client = await new ApolloClient({
      ssrMode: true,
      link: await authLink.concat(httpLink),
      cache: new InMemoryCache()
    });

    return client;
  } catch (err) {
    throw new Error(err);
  }
};
