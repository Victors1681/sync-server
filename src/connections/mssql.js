require("dotenv").config();
import sql from "mssql";

const config = {
  user: process.env.SQL_USER,
  password: process.env.SQL_PASSWORD,
  server: process.env.SQL_SERVER,
  database: process.env.SQL_DB
};

const mssqlQuery = async query => {
  try {
    const pool = await sql.connect(config);
    const result = await pool
      .request()
      // .input(input)
      .query(query);

    await sql.close();
    return result.recordset;
  } catch (error) {
    throw new Error(error);
  }
};

export const mssqlExecute = async storedProcedure => {
  try {
    const pool = await sql.connect(config);

    let result = await pool.request().execute(storedProcedure);
    await sql.close();
    return result.recordset;
  } catch (error) {
    throw new Error(error);
  }
};

export default mssqlQuery;
