require("dotenv").config();
import "graphql-import-node";
import apolloClient from "./apolloClient";
import login from "./login";
import getClients from "./handlers/clients";
import getProducts from "./handlers/products";
import getSellers from "./handlers/seller";
import getInvoices from "./handlers/invoices";
import SYNC_ALL from "./graphql/SyncAll.graphql";
import sqlLoader from "sql-loader";

const serverQuery = async () => {
  try {
    await login(); // login and save token local storage
    const sql = sqlLoader("./sql");

    // const products = await getProducts(sql);
    // const clients = await getClients(sql);
    // const sellers = await getSellers(sql);

    // const aClient = await apolloClient();
    console.log("before>..");
    const invoices = await getInvoices();

    // const response = await aClient.mutate({
    //   mutation: SYNC_ALL,
    //   variables: { products, clients, sellers }
    // });

    //  console.log(response);
  } catch (err) {
    throw new Error(err);
  }
};

serverQuery();
