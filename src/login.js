import apolloClient from "./apolloClient";
import gql from "graphql-tag";
import localStorage from "localStorage";

const LOGIN_QL = gql`
  mutation loginSystem($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`;

const getToken = async () => {
  try {
    const user = process.env.SERVER_EMAIL;
    const pass = process.env.SERVER_PASSWORD;

    const client = await apolloClient(true);

    const response = await client.mutate({
      mutation: LOGIN_QL,
      variables: {
        email: user,
        password: pass
      }
    });

    if (response && response.data && response.data.login) {
      const { token } = response.data.login;
      await localStorage.setItem("token", token);

      console.log("Token Saved!");
    }
  } catch (err) {
    console.log(err);
    throw new Error("Unable to login and retrieve the token", err);
  }
};

module.exports = getToken;
